package action;

/**
 * Different types of actions that can be performed on a unit.
 *
 */
public enum UnitActionType {
	ATTACK, MOVE, HEAL, SWAP, MOVE_CRYSTAL;
}
