package action;

/**
 * Action signifying the end of the turn for a player.
 *
 */
public class EndTurnAction extends Action {

	@Override
	public String toString() {
		return "EndTurnAction []";
	}
	
}
