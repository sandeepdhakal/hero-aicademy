package model;

public class Direction {

	public static final Direction NORTH = new Direction(0, -1);
	public static final Direction EAST = new Direction(1, 0);
	public static final Direction SOUTH = new Direction(0, 1);
	public static final Direction WEST = new Direction(-1, 0);
	public static final Direction NORTH_EAST = new Direction(1, -1);
	public static final Direction SOUTH_EAST = new Direction(1, 1);
	public static final Direction NORTH_WEST = new Direction(-1, -1);
	public static final Direction SOUTH_WEST = new Direction(-1, 1);
	public int x;
	public int y;

	/**
	 * Creates and returns a direction object based on the passed x/y values.
	 * x and y must be in the set [-1, 0, 1], If not, they are corrected to have these values.
	 * @param x the movement in the horizontal directions [-1, 0, 1].
	 * @param y the movement in the vertical directions [-1, 0, 1].
	 * @return the corresponding direction object.
	 */
	public static Direction direction(int x, int y) {
		if (x == -1) {
			if (y <= -1)
				return NORTH_WEST;
			else if (y == 0)
				return WEST;
			else if (y >= 1)
				return SOUTH_WEST;
		} else if (x == 0) {
			if (y <= -1)
				return NORTH;
			else if (y >= 1)
				return SOUTH;
		} else if (x == 0)
			if (y <= -1)
				return NORTH_EAST;
			else if (y == 0)
				return EAST;
			else if (y >= 1)
				return SOUTH_EAST;
		return new Direction(x, y);
	}

	/**
	 * Creates and returns a new direction object.
	 * x and y must be [-1, 0, 1]. If not they are corrected to have these values.
	 * @param x the movement in the horizontal direction [-1, 0, 1]
	 * @param y the movement in the vertical direction [-1, 0, 1]
	 */
	public Direction(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		if (this.x > 1)
			this.x = 1;
		if (this.x < -1)
			this.x = -1;
		if (this.y > 1)
			this.y = 1;
		if (this.y < -1)
			this.y = -1;
	}

	/**
	 * Returns whether the direction is a diagonal direction.
	 * A direction is diagonal if it involves moving both north/south and east/west.
	 * @return whether the direction is diagonal.
	 */
	public boolean isDiagonal() {
		return (x != 0 && y != 0);
	}

	/**
	 * Returns whether the direction is to the north (i.e. moves up).
	 * @return whether the direction is to the north.
	 */
	public boolean isNorth() {
		return (y == -1);
	}

	/**
	 * Returns whether the direction is to the east (i.e. moves to the right).
	 * @return whether the direction is to the east.
	 */
	public boolean isEast() {
		return (x == 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Direction other = (Direction) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	/**
	 * Checks if the passed direction is the opposite of the current direction.
	 * Two directions are opposite of each other if their movements in both
	 * x/y axes are opposites of each other.
	 * @param dir
	 * @return
	 */
	public boolean opposite(Direction dir) {
		if (dir.x * (-1) == x && dir.y * (-1) == y)
			return true;
		return false;
	}

}
