package model;

import java.util.Arrays;

public class CardSet {

	/**
	 * all the available cards. index is the ordinal value of Card and
	 * value is the number of that type of card
	 */
	public int[] cards;
	
	/**
	 * the number of cards in the set
	 */
	public int size;
	
	/**
	 * the seed used to randomly draw a card from the list of available cards.
	 */
	public int seed;

	/**
	 * Initialise the card set with all the available cards.
	 * A random seed is generated. The initial size of the card set is 0.
	 */
	public CardSet() {
		cards = new int[Card.values().length];
		size = 0;
		seed = (int) (Math.random() * 1000);
	}

	/**
	 * Initialise the card set with all the available cards.
	 * The initial size of the card set is 0.
	 * @param seed the seed used to draw a card from the set of available cards.
	 */
	public CardSet(int seed) {
		cards = new int[Card.values().length];
		size = 0;
		this.seed = seed;
	}

	/**
	 * Get a card from the card set as determined by the seed.
	 * @return the card that was retrieved from the card set.
	 */
	public Card determined() {
		if (seed > 100000)
			seed = (int) (seed * 0.1);
		else
			seed = (int) (seed * 1.3 + 7);
		return get(seed % size);
	}

	/**
	 * Get a random card from the card set, ignoring the seed.
	 * @return the card that was retrieved from the card set.
	 */
	public Card random() {
		return get((int) Math.floor(Math.random() * size));
	}

	/**
	 * Get a card from the card set.
	 * @param r the rth card in the card set to return
	 * @return the rth card in the card set.
	 */
	public Card get(Integer r) {
		int c = 0;	// temp count of the  number of cards
		int i = 0; // looping through each card type in the card set
		while (true) {
			c += cards[i];
			if (c > r)
				break;
			i++;
		}
		return Card.values()[i];
	}

	/**
	 * Add a new card to the list of available cards.
	 * A new type of card is not added, rather the number of the specified card is incremented by 1.
	 * @param card the card to be added.
	 */
	public void add(Card card) {
		// increase the number of this card
		cards[card.ordinal()]++;
		size++;
	}

	/**
	 * Remove a card from the list of available cards.
	 * A card type is not entirely removed, rather the number of the specified card is reduced by 1.
	 * @param card the card to remove.
	 */
	public void remove(Card card) {
		if (cards[card.ordinal()] > 0) { // if the card exists in the set
			cards[card.ordinal()]--;
			size--;
		}
	}

	/**
	 * Returns whether the card set contains any units.
	 * @return true if the card set has at least one unit, false otherwise.
	 */
	public boolean hasUnits() {
		if (units() > 0)
			return true;
		return false;
	}

	/**
	 * Add call the cards from another card set.
	 * @param other the card set to augment this card set with.
	 */
	public void addAll(CardSet other) {
		size += other.size;
		for (int i = 0; i < other.cards.length; i++)
			cards[i] += other.cards[i];
	}

	/**
	 * Remove all cards. This means resetting the count of each card to 0.
	 */
	public void clear() {
		size = 0;
		Arrays.fill(cards, 0);
	}

	/**
	 * Returns if the card set contains no cards.
	 * @return if the card set contains no cards.
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Imitate the card set and seed of the supplied card set
	 * @param p1Hand the card set to imitate.
	 */
	public void imitate(CardSet p1Hand) {
		clear();
		addAll(p1Hand);
		seed = p1Hand.seed;
	}

	/**
	 * Counts the number of units (excluding crystals) in the card set.
	 * @return the number of units.
	 */
	public int units() {
		int units = 0;
		for (final Card card : Card.values())
			if (card.type == CardType.UNIT && card != Card.CRYSTAL)
				units += cards[card.ordinal()];
		return units;
	}
	

	/**
	 * Checks whether the card set contains at least one card of this type.
	 * @param card the type of card to check.
	 * @return yes if at least one of the card type is present, false otherwise.
	 */
	public boolean has(Card card) {
		if (cards.length > card.ordinal())
			return cards[card.ordinal()] > 0;
		return false;
	}

	/**
	 * Checks whether the card set contains at least one card of this type.
	 * TODO: check if this function can replace the above function??
	 * @param card the type of card to check.
	 * @return yes if at least one of the card type is present, false otherwise.
	 */
	public boolean contains(Card card) {
		if (cards.length > card.ordinal())
			return cards[card.ordinal()] > 0;
		return false;
	}

	/**
	 * Return the number of cards of this type in the card set.
	 * @param card the type of card to check.
	 * @return the number of cards of this type.
	 */
	public int count(Card card) {
		if (cards.length > card.ordinal())
			return cards[card.ordinal()];
		return 0;
	}

	@Override
	public int hashCode() {
		int hash = 1;
		final int prime = 7;
		for (int card : cards)
			hash = hash * prime + card;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CardSet other = (CardSet) obj;
		if (!Arrays.equals(cards, other.cards))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Arrays.toString(cards).replaceAll(" ", "");
	}
}
