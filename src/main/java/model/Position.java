package model;

/**
 * A class representing a position within the game map.
 *
 */
public class Position {

	/**
	 * position in the horizontal direction.
	 */
	public int x;
	
	/**
	 * position in the vertical direction.
	 */
	public int y;

	/**
	 * Instantiate a new Position object.
	 * @param x position in the horizontal direction.
	 * @param y position in the vertical direction.
	 */
	public Position(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Instantiate a new position object at the top-left of the map
	 * Both x/y = 0.
	 */
	public Position() {
		super();
		x = 0;
		y = 0;
	}

	/**
	 * Determine the direction of movement from this position to the specified position.
	 * @param pos the position to get the direction for.
	 * @return the direction from this position to the specified position.
	 */
	public Direction getDirection(Position pos) {
		if (pos == null)
			return null;
		return Direction.direction(pos.x - x, pos.y - y);
	}

	/**
	 * Get the hash code of this position.
	 * @return the hash code of this position.
	 */
	public int hashCode() {
		int result = 1;
		result = 5 * result + x;
		result = 5 * result + y;
		return result;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public int distance(Position to) {
		int xx = x - to.x;
		if (xx < 0)
			xx = xx * (-1);
		int yy = y - to.y;
		if (yy < 0)
			yy = yy * (-1);
		return xx + yy;
	}

}
