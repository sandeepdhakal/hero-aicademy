package model;


/**
 * Class to represent a unit/character in the game.
 * This is different from Unit, which represents the current status of the unit/character.
 */
public class UnitClass {

	/**
	 * the card corresponding to this unit.
	 */
	public Card card;
	
	/**
	 * the max health power of this unit.
	 */
	public int maxHP;
	
	/**
	 * the distance that can be travelled by this unit.
	 */
	public int speed;
	
	/**
	 * the resistance offered by this unit in case of a physical attack.
	 */
	public int physicalResistance;
	
	/**
	 * the resistance offered by this unit in case of a magical attack.
	 */
	public int magicalResistance;
	public int power;
	
	/**
	 * the attacking information of this unit.
	 */
	public Attack attack;
	
	/**
	 * the healing information (power and range) of this unit.
	 */
	public Heal heal;
	public boolean swap;

	public UnitClass(Card card, int maxHP, int speed, int power,
			int physicalResistance, int magicalResistance, Attack attack,
			Heal heal, boolean swap) {
		super();

		this.card = card;
		this.maxHP = maxHP;
		this.speed = speed;
		this.power = power;
		this.attack = attack;
		this.heal = heal;
		this.physicalResistance = physicalResistance;
		this.magicalResistance = magicalResistance;
		this.swap = swap;
	}

	/**
	 * Generate a hash of this unit class.
	 * @return the hash of this unit class.
	 */
	public int hash() {
		switch(card){
		case ARCHER : return 0;
		case CRYSTAL : return 1;
		case KNIGHT : return 2;
		case NINJA : return 3;
		case CLERIC : return 4;
		case WIZARD : return 5;
		}
		return 6;
	}
}
