package model;

/**
 * The size of the deck (Standard, Small or Tiny)
 *
 */
public enum DECK_SIZE {
	STANDARD, SMALL, TINY;
}
