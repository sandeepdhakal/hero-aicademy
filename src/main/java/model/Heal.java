package model;

/**
 * Healing power for the cleric character.
 */
public class Heal {

	/**
	 * the range within which another character can be healed.
	 */
	public int range;
	
	/**
	 * the healing power of the healer.
	 */
	public int heal;
	
	/**
	 * the reviving power of the healer
	 */
	public int revive;

	public Heal(int range, int heal, int revive) {
		super();
		this.range = range;
		this.heal = heal;
		this.revive = revive;
	}

}
