package model;

import java.util.ArrayList;
import java.util.List;

public class HaMap {

	/**
	 * the name of the map
	 */
	public String name;
	
	/**
	 * the number of squares in each row
	 */
	public int width;
	
	/**
	 * the number of squares in each column
	 */
	public int height;
	
	/**
	 * the type of square in each (x,y) position on the map
	 */
	public SquareType[][] squares;
	
	/**
	 * the assault squares on the map
	 */
	public List<Position> assaultSquares;
	
	/**
	 * the squares where player 1 can deploy units on the map
	 */
	public List<Position> p1DeploySquares;
	
	/**
	 * the squares where player 2 can deploy units on the map
	 */
	public List<Position> p2DeploySquares;
	
	/**
	 * the squares where player 1 has its crystals.
	 */
	public List<Position> p1Crystals;
	
	/**
	 * the squares where player 1 has its crystals.
	 */
	public List<Position> p2Crystals;

	public HaMap(int width, int height, SquareType[][] squares, String name) {
		super();
		this.name = name;
		this.width = width;
		this.height = height;
		this.squares = squares;
		assaultSquares = new ArrayList<Position>();
		p1DeploySquares = new ArrayList<Position>();
		p2DeploySquares = new ArrayList<Position>();
		p1Crystals = new ArrayList<Position>();
		p2Crystals = new ArrayList<Position>();
		for (int x = 0; x < squares.length; x++)
			for (int y = 0; y < squares[0].length; y++) {
				if (squares[x][y] == SquareType.DEPLOY_1)
					p1DeploySquares.add(new Position(x, y));
				if (squares[x][y] == SquareType.DEPLOY_2)
					p2DeploySquares.add(new Position(x, y));
				if (squares[x][y] == SquareType.ASSAULT)
					assaultSquares.add(new Position(x, y));
			}
	}

	/**
	 * Get the type of square at a particular position in the map.
	 * @param x the position of the square in the horizontal direction
	 * @param y the position of the square in the vertical direction
	 * @return the type of the square at the (x, y) position.
	 */
	public SquareType squareAt(int x, int y) {

		return squares[x][y];

	}

	/**
	 * Get the type of square at the specified position.
	 * @param position the position of the square
	 * @return the type of the square
	 */
	public SquareType squareAt(Position position) {
		return squareAt(position.x, position.y);
	}
}
