package model;

public class Attack {

	/**
	 * the range of the attack. the attack cannot be launched beyond this range.
	 * this is a distance in terms of cells on the board.
	 */
	public int range;
	
	/**
	 * the type of the attack.
	 */
	public AttackType attackType;
	
	/**
	 * the damage done to the opponent unit is multiplied by this value.
	 * the opponent has to be on a cell adjacent to the attacker.
	 */
	public double meleeMultiplier;
	
	/**
	 * the damage done to the opponent is multiplied by this value.
	 * the opponent is not on a cell adjacent to the attacker.
	 */
	public double rangeMultiplier;
	

	public boolean chain;
	public boolean push;
	
	/**
	 * Initialise a new Attack object.
	 * @param range the range of the attack.
	 * @param attackType the type of the attack.
	 * @param damage the damage that will be done to the opponent by this attack.
	 * @param meleeMultiplier a multiplier to the damage done to the opponent on an adjacent cell.
	 * @param rangeMultiplier a multiplier to the damage done to the opponent on a non-adjacent cell.
	 * @param chain 
	 * @param push
	 */
	public Attack(int range, AttackType attackType,
			int damage, double meleeMultiplier, double rangeMultiplier,
			boolean chain, boolean push) {
		super();
		this.range = range;
		this.attackType = attackType;
		this.meleeMultiplier = meleeMultiplier;
		this.rangeMultiplier = rangeMultiplier;
		this.chain = chain;
		this.push = push;
		
	}

	
}
