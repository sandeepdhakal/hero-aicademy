package model;


/**
 * The type of squares available in the game map.
 * The game map is composed of squares.
 *
 */
public enum SquareType {

    /**
     * an empty square
     */
    NONE, 

    /**
     * any character standing on this square gets a special assault bonus
     */
    ASSAULT,

    /**
     * any character standing on this square gets a sepcial defense bonus
     */
    DEFENSE,

    /**
     * any character standing on this square gets a power bonus
     */
    POWER, 

    /**
     * square where player 1's characters will be deployed initially
     */
    DEPLOY_1,

    /**
     * square where player 2's characters will be deployed initially
     */
    DEPLOY_2;
	
}
