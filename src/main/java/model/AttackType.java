package model;

/**
 * The type of attack that can be done (Physical or Magical).
 *
 */
public enum AttackType {

	Physical, Magical;
	
}
