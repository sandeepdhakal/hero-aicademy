package model;

/**
 * The type of a card (Unit, Item or Spell).
 *
 */
public enum CardType {
	UNIT, 
	ITEM,
	SPELL;
}

