package experiments;

import java.io.PrintWriter;
import java.util.Properties;

import game.Game;
import game.GameArguments;
import game.GameState;

public class APComparison {

	public static void main(String[] args) {
		Properties config = GameArguments.readConfigFile(args[0]);
		GameArguments gameArgs = GameArguments.fromProperties(config);

		int games = Integer.parseInt(config.getProperty("games", "1"));

		StringBuilder out = new StringBuilder();
		out.append("-----------------------" + "\n");
		out.append("Proc. cores: ").append(Runtime.getRuntime().availableProcessors()).append("\n");
		out.append("Budget: ").append(gameArgs.budget).append("\n");
		out.append("AP: ").append(GameState.ACTION_POINTS).append("\n");
		out.append("TURN LIMIT: ").append(GameState.TURN_LIMIT).append("\n");
		out.append("Games: ").append(games).append("\n");
		out.append("P1: ").append(config.get("p1")).append("\n");
		out.append("P2: ").append(config.get("p2")).append("\n");
		out.append("Move crystal cards: ").append(gameArgs.numMoveCrystals).append("\n");
		out.append("Crystal move distance: ").append(GameState.CRYSTAL_MOVE_DISTANCE).append("\n");
		out.append("Randomly move crystal: ").append(gameArgs.randomlyMoveCrystals).append("\n");
		out.append("Crystal move probability: ").append(gameArgs.crystalMoveProbability).append("\n");
		out.append("Crystal heal power: ").append(GameState.CRYSTAL_HEAL_PROPORTION).append("\n");
		out.append("Deck size: ").append(gameArgs.deckSize).append("\n");
		System.out.println("Running: \n" + out);
		out.append("\n");

		int p1Wins = 0;
		int p2Wins = 0;
		int draws = 0;

		for (int i = 0; i < games; i++) {
			Game game = new Game(null, gameArgs);
			game.run();
			if (game.state.getWinner() == 1) {
				p1Wins += 1;
			} else if (game.state.getWinner() == 2) {
				p2Wins += 1;
			} else {
				draws += 1;
			}
			int winner = game.state.getWinner();
			if (winner > 0) {
				out.append(winner).append(" (").append(game.state.turn).append(", ").append(game.state.winMethod).append(")\n");
			} else {
				out.append("draw\n");
			}
		}

		out.append("p1: ").append(p1Wins).append("\n");
		out.append("p2: ").append(p2Wins).append("\n");
		out.append("Draws: ").append(draws).append("\n");
		out.append("-----------------------" + "\n");

		try {
			String outputFname = args[0].replace("config", "txt");
			/*String outputFname = config.getProperty("map") + "_"
                                    + config.getProperty("deck_size") + "_"
                                    + config.getProperty("p1") + "_" + config.getProperty("p2")
                                    + "_b" + config.getProperty("budget")
                                    + "_ap" + config.getProperty("ap")
                                    + "_g" + config.getProperty("games")
                                    + "_cc" + config.getProperty("move_crystal_cards")
                                    + "_cmd" + config.getProperty("crystal_move_distance")
                                    + ".txt";
			 */

			PrintWriter writer = new PrintWriter(outputFname);
			writer.println(out);
			writer.close();
		} catch (Exception e){
			System.out.println("Could not save file!" + e.getMessage());
		}

		System.out.println(out);
		System.out.println("Done");
	}
}
