package game;

import model.DECK_SIZE;
import ai.AI;
import ai.GreedyActionAI;
import ai.GreedyTurnAI;
import ai.HeuristicAI;
import ai.HybridAI;
import ai.NmSearchAI;
import ai.RandomAI;
import ai.RandomHeuristicAI;
import ai.RandomSwitchAI;
import ai.evaluation.HeuristicEvaluator;
import ai.evaluation.LeafParallelizer;
import ai.evaluation.MaterialBalanceEvaluator;
import ai.evaluation.RolloutEvaluator;
import ai.evaluation.LeafParallelizer.LEAF_METHOD;
import ai.evolution.OnlineIslandEvolution;
import ai.evolution.OnlineEvolution;
import ai.mcts.Mcts;
import ai.util.RAND_METHOD;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class GameArguments {
	
	public final AI[] players;
	public String mapName;
	public DECK_SIZE deckSize = DECK_SIZE.STANDARD;
	public int sleep;
	public boolean gfx;
	
	/**
	 * number of milliseconds available for a turn
	 */
	public int budget = 6000;
	
	/**
	 * the player for which these arguments are provided.
	 * p = 0: player #1
	 * p = 1: player #2
	 */
	private int p;
	
	/**
	 * whether or not a map argument is provided
	 */
	private boolean m = false;
	
	/**
	 * whether or not a deck size argument is provided
	 */
	private boolean d = false;
	
	/**
	 * whether or not an argument for the number of action points is provided
	 */
	private boolean aa = false;

	/**
	 * how many move crystal cards to add to each player's deck
	 */
	public int numMoveCrystals = 0;

	/**
	 * whether crystals will be randomly moved after `turnsToWaitForMovingCrystals` turns have been played.
	 */
	public boolean randomlyMoveCrystals = false;

	/**
	 * the number of turns to wait before randomly moving crystals if it is allowed.
	 */
	public int turnsToWaitForMovingCrystals = 20;

	/**
	 * the probability of a crystal being moved randomly after `turnsToWaitForMovingCrystals` turns have been played.
	 */
	public double crystalMoveProbability = 0.0;

	/**
	 * the proportion of the difference between a crystal's current health and full health
	 * that is healed when it moves.
	 */
	public double crystalHealValue = 0.0;
	
	public GameArguments(String[] args) {
		players = new AI[2];
		mapName = "a";
		p = -1;
		sleep = 40;
		gfx = true;
		setup(args);
	}
	
	public GameArguments(boolean gfx, AI p1, AI p2, String mapName, DECK_SIZE deckSize) {
		players = new AI[2];
		players[0] = p1;
		players[1] = p2;
		this.mapName = mapName;
		p = -1;
		this.gfx = gfx;
		sleep = gfx ? 40 : 0;
		this.deckSize = deckSize;
	}

	public GameArguments(boolean gfx, AI p1, AI p2, String mapName, DECK_SIZE deckSize, int ap) {
		players = new AI[2];
		players[0] = p1;
		players[1] = p2;
		this.mapName = mapName;
		p = -1;
		this.gfx = gfx;
		sleep = gfx ? 40 : 0;
		this.deckSize = deckSize;
		setAP(ap);
	}

	private void setAP(int ap) {
		GameState.STARTING_AP = Math.max(1, ap - 1);
		GameState.ACTION_POINTS = ap;
		GameState.TURN_LIMIT = 100 * Math.max(1, (6-ap));
	}

	private void setup(String[] args) {
		AI randomSwitch = new RandomSwitchAI(0.5, new HeuristicAI(), new RandomAI(RAND_METHOD.TREE));
		for (int a = 0; a < args.length; a++) {
			if (args[a].equalsIgnoreCase("map")) {
				m = true;
				continue;
			}
			if (args[a].equalsIgnoreCase("ap")) {
				aa = true;
				continue;
			}
			if (args[a].equalsIgnoreCase("deck")) {
				d = true;
				continue;
			}
			if (args[a].equalsIgnoreCase("p1")) {
				p = 0;
				continue;
			} else if (args[a].equalsIgnoreCase("p2")) {
				p = 1;
				continue;
			}
			if (m){
				mapName = args[a];
				m = false;
				continue;
			}
			if (aa){
				setAP(Integer.parseInt(args[a]));
				aa = false;
				continue;
			}
			if (d){
				if (args[a].equals("standard"))
					deckSize = DECK_SIZE.STANDARD;
				if (args[a].equals("small"))
					deckSize = DECK_SIZE.SMALL;
				if (args[a].equals("tiny"))
					deckSize = DECK_SIZE.TINY;
				d = false;
				continue;
			}
			if (p == 0 || p == 1) {
				if (args[a].equalsIgnoreCase("human"))
					players[p] = null;
				else if (args[a].equalsIgnoreCase("random"))
					players[p] = new RandomAI(RAND_METHOD.TREE);
				if (args[a].equalsIgnoreCase("greedyaction")) {
					players[p] = new GreedyActionAI(new HeuristicEvaluator(false));
				}
				if (args[a].equalsIgnoreCase("greedyturn")) {
					players[p] = new GreedyTurnAI(new HeuristicEvaluator(false), budget);
				}
				if (args[a].equalsIgnoreCase("mcts-vanilla")) {
					players[p] = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(0.5), new HeuristicEvaluator(true)));
				}
				if (args[a].equalsIgnoreCase("mcts-ne")) {
					players[p] = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(1), new HeuristicEvaluator(true)));
					((Mcts)players[p]).c = 0;
				}
				if (args[a].equalsIgnoreCase("mcts-cut")) {
					players[p] = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(.5), new HeuristicEvaluator(true)));
					((Mcts)players[p]).cut = true;
				}
				if (args[a].equalsIgnoreCase("mcts-collapse")) {
					players[p] = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(0), new HeuristicEvaluator(true)));
					((Mcts)players[p]).collapse = true;
				}
				if (args[a].equalsIgnoreCase("online-evolution")){
					players[p] = new OnlineIslandEvolution(false, 100, .33, .66, budget, new RolloutEvaluator(1, 1, randomSwitch, new HeuristicEvaluator(false)));
				}
				p = -1;
			} else if (args[a].equalsIgnoreCase("sleep")) {
				a++;
				sleep = Integer.parseInt(args[a]);
			} else if (args[a].equalsIgnoreCase("gfx")) {
				a++;
				gfx = Boolean.parseBoolean(args[a]);
			} else if (args[a].equalsIgnoreCase("budget")) {
				a++;
				budget = Integer.parseInt(args[a]);
			}
		}
	}

	public static Properties readConfigFile(String filename) {
		Properties prop = new Properties();
		try (FileInputStream fis = new FileInputStream(filename)) {
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop;
	}

	public static GameArguments fromProperties(Properties config) {
		int budget = Integer.parseInt(config.getProperty("budget", "6000"));
		int ap = Integer.parseInt(config.getProperty("ap", "5"));
		int moveCrystalCards = Integer.parseInt(config.getProperty("move_crystal_cards", "0"));
		String map = config.getProperty("map");

		AI mcts = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(0.5), new HeuristicEvaluator(true)));

		AI cutting = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(0.5), new HeuristicEvaluator(true)));
		((Mcts)cutting).cut = true;

		AI noneExp = new Mcts(budget, new RolloutEvaluator(1, 1, new RandomHeuristicAI(1), new HeuristicEvaluator(true)));
		((Mcts)noneExp).c = 0;

		AI onlineE = new OnlineEvolution(true, 100, 0.1, 0.5, budget, new HeuristicEvaluator(false));

		String p1Name = config.getProperty("p1").toLowerCase();
		String p2Name = config.getProperty("p2").toLowerCase();

		AI p1 = null;
		AI p2 = null;
		switch (p1Name) {
			case "cutting": p1 = cutting; break;
			case "non": p1 = noneExp; break;
			case "oe": p1 = onlineE; break;
			case "mcts": p1 = mcts;
		}
		switch (p2Name) {
			case "cutting": p2 = cutting; break;
			case "non": p2 = noneExp; break;
			case "oe": p2 = onlineE; break;
			case "mcts": p2 = mcts;
		}
		if (p1 == null || p2 == null) return null;

		// deck size
		String decksize = config.getProperty("deck_size").toLowerCase();
		DECK_SIZE deckSize = null;
		switch (decksize) {
			case "tiny": deckSize = DECK_SIZE.TINY; break;
			case "small": deckSize = DECK_SIZE.SMALL; break;
			case "standard": deckSize = DECK_SIZE.STANDARD; break;
		}
		if (deckSize == null) return null;

		//TODO: move settings to a single source
		GameArguments gameArgs = new GameArguments(false, p1, p2, map, deckSize, ap);
		gameArgs.budget = budget;
		gameArgs.numMoveCrystals = moveCrystalCards;

		gameArgs.randomlyMoveCrystals = Boolean.parseBoolean(config.getProperty("randomly_move_crystals", "false"));
		gameArgs.turnsToWaitForMovingCrystals = Integer.parseInt(config.getProperty("turns_to_wait_before_moving_crystals", "20"));
		gameArgs.crystalMoveProbability = Double.parseDouble(config.getProperty("crystal_move_probability", "0"));
		gameArgs.crystalHealValue = Double.parseDouble(config.getProperty("crystal_heal_value", "0"));
		GameState.CRYSTAL_HEAL_PROPORTION = gameArgs.crystalHealValue;

		GameState.CRYSTAL_MOVE_DISTANCE = Integer.parseInt(config.getProperty("crystal_move_distance", "1"));
		GameState.NUM_MOVE_CRYSTALS = moveCrystalCards;

		return gameArgs;
	}
}
