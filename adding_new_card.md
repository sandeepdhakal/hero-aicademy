# Adding a new card to the game
1. Add new card enum to model.Card class
2. Add action to the action.SingletonAction class
3. Add the card to Council.java
4. Update UI.java to load UI for added Card
5. Update possible actions for that card in the game.GameState class: possibleActions(Card card, List<Action> actions)
6. Update method *update(Action actions)* corresponding to the action by the card.


## Add a new card to move crystals: MOVE_CRYSTAL
### Adding a new action
GameState.java
public void possibleActions(Unit unit, Position from, List<Action> actions) {
...
    if (unit.unitClass.card == Card.CRYSTAL)
	1. check if current hand contains MOVE_CRYSTAL card
	2. if yes, allow the crystal to be moved to one of the adjoining empty squares (d = 1)
    }
...

    
}
